#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<opencv2/opencv.hpp>
#include<pthread.h>

using namespace cv;
using namespace std;

int main()
{
  VideoCapture  cap(0);
  if(!cap.isOpened())
  {
    cout << "Open" << endl;
  }
  namedWindow("LiveWeb");

  while(char(waitKey(1)) != 'q' && cap.isOpened())
  {
    Mat frame;
    cap >> frame;
    if(frame.empty())
    {
      cout << "The End" << endl;
      break;
    }
    imshow("Video",frame);
  }
}